use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use std::thread;
use std::time::Duration;
use std::{io::stdout, process::ExitCode};

use anyhow::{Context, Result};
use clap::Parser;
use directories::BaseDirs;
use log::LevelFilter;
use rand::{seq::SliceRandom, thread_rng};
use rss::Channel;

mod cli;

const WORD: &str = "https://www.oed.com/rss.xml";
const NEWS: &str = "http://feeds.bbci.co.uk/news/uk/rss.xml";
const ICONCOLOUR: &str = "#d08770";

type ThingGetter<S> = fn(usize) -> Result<(S, S)>;

#[derive(Debug)]
struct Feeder<'a, P, W, S> {
    getters: &'a [ThingGetter<S>],
    ok_delay: Duration,
    err_delay: Duration,
    cache_file: P,
    word_wrap: Option<u64>,
    output: &'a mut W,
}

impl<'a, P, O, S> Feeder<'a, P, O, S>
where
    P: AsRef<Path>,
    O: Write,
    S: AsRef<[u8]> + std::fmt::Debug,
{
    fn new(
        getters: &'a [ThingGetter<S>],
        ok_delay: Duration,
        err_delay: Duration,
        cache_file: P,
        word_wrap: Option<u64>,
        output: &'a mut O,
    ) -> Self {
        Feeder {
            getters,
            ok_delay,
            err_delay,
            cache_file,
            output,
            word_wrap,
        }
    }

    fn run(&mut self) -> Result<()> {
        let mut rng = thread_rng();
        let mut possible_items: Vec<usize> = (0..7).collect();
        loop {
            possible_items.shuffle(&mut rng);
            for item in &possible_items {
                let results: Vec<Result<(S, S)>> = self.getters.iter().map(|f| f(*item)).collect();
                self.process_things(results)?;
            }
        }
    }

    fn process_things(&mut self, results: Vec<Result<(S, S)>>) -> Result<()> {
        let (things, fails): (Vec<_>, Vec<_>) = results.into_iter().partition(|x| x.is_ok());
        fails
            .into_iter()
            .for_each(|fail| log::warn!("{}", fail.unwrap_err()));

        match things.is_empty() {
            true => {
                log::error!("All RSS requests failed");
                thread::sleep(self.err_delay);
            }
            false => self.print(&things.into_iter().map(|x| x.unwrap()).collect::<Vec<_>>())?,
        }
        Ok(())
    }

    fn print<T>(&mut self, input: &[(T, T)]) -> Result<()>
    where
        T: AsRef<[u8]>,
    {
        for (item, webpage) in input {
            let mut bytes = item.as_ref().to_owned();
            if let Some(length) = self.word_wrap {
                if bytes.len() > length as usize {
                    bytes = bytes
                        .into_iter()
                        .take(length as usize - 3)
                        .chain(b"...".to_owned())
                        .collect::<Vec<_>>();
                }
            }
            self.output.write_all(&bytes)?;
            self.output.write_all(&[b'\n'])?;
            OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(&self.cache_file)?
                .write_all(webpage.as_ref())?;
            log::info!("Waiting for {:#?}", self.ok_delay);
            thread::sleep(self.ok_delay)
        }
        Ok(())
    }
}

fn word_of_day(_: usize) -> Result<(String, String)> {
    let content = reqwest::blocking::get(WORD)?.bytes()?;
    let channel = Channel::read_from(&content[..])?;
    let item = channel
        .items()
        .last()
        .context("Couldn't get today's entry")?;
    let webpage = match item.link() {
        Some(url) => format!("<meta http-equiv=\"refresh\" content=\"0; url={url}\">"),
        None => "<p> No link for item </p>".to_owned(),
    };
    Ok((
        format!(
            "%{{F{ICONCOLOUR}}}%{{F-}}  {}",
            item.description()
                .context("Couldn't get definition")?
                .replace("OED Word of the Day: ", "")
        ),
        webpage,
    ))
}

fn news(n: usize) -> Result<(String, String)> {
    let content = reqwest::blocking::get(NEWS)?.bytes()?;
    let channel = Channel::read_from(&content[..])?;
    let item = channel
        .items()
        .iter()
        .nth(n)
        .with_context(|| format!("Couldn't get news item {n}"))?;
    let webpage = match item.link() {
        Some(url) => format!("<meta http-equiv=\"refresh\" content=\"0; url={url}\">"),
        None => "<p> No link for item </p>".to_owned(),
    };
    Ok((
        format!(
            "%{{F{ICONCOLOUR}}}%{{F-}}  {}",
            item.title().context("Couldn't get news title")?
        ),
        webpage,
    ))
}

fn run(delay: Duration, word_wrap: Option<u64>, output: &mut impl Write) -> Result<()> {
    let cache_file = BaseDirs::new()
        .context("Failed to locate config dir")?
        .cache_dir()
        .join(env!("CARGO_PKG_NAME"));
    let mut feeder = Feeder::new(
        &[news, word_of_day],
        delay,
        Duration::from_secs(60),
        cache_file,
        word_wrap,
        output,
    );
    feeder.run()
}

fn main() -> ExitCode {
    let options = cli::Options::parse();
    let level = match options.verbosity {
        0 => LevelFilter::Warn,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    env_logger::Builder::new().filter_level(level).init();

    if let Err(err) = run(options.delay.into(), options.wordwrap, &mut stdout()) {
        log::error!("{err:?}");
        return ExitCode::FAILURE;
    }
    ExitCode::SUCCESS
}

#[cfg(test)]
mod tests {
    use super::*;
    use anyhow::anyhow;
    use tempfile::NamedTempFile;

    fn good_scrape(_: usize) -> Result<(String, String)> {
        Ok(("Hi".to_owned(), "url".to_owned()))
    }

    fn bad_scrape(_: usize) -> Result<(String, String)> {
        Err(anyhow!("Err"))
    }

    #[test]
    fn test_printer() {
        let mut output = std::io::Cursor::new(vec![]);
        let cache_file = NamedTempFile::new().unwrap();
        let mut feeder: Feeder<_, std::io::Cursor<Vec<u8>>, String> = Feeder::new(
            &[],
            Duration::from_millis(1),
            Duration::from_millis(1),
            cache_file,
            None,
            &mut output,
        );
        feeder.print(&[("Hi", "url"), ("Oh", "url")]).unwrap();
        assert_eq!(output.get_ref(), "Hi\nOh\n".as_bytes());
    }

    #[test]
    fn process_ok_ok() {
        let mut output = std::io::Cursor::new(vec![]);
        let cache_file = NamedTempFile::new().unwrap();
        let mut feeder: Feeder<_, std::io::Cursor<Vec<u8>>, String> = Feeder::new(
            &[],
            Duration::from_millis(1),
            Duration::from_millis(1),
            cache_file,
            None,
            &mut output,
        );
        feeder
            .process_things(vec![good_scrape(0), good_scrape(0)])
            .unwrap();
        assert_eq!(output.get_ref(), "Hi\nHi\n".as_bytes());
    }

    #[test]
    fn process_ok_err() {
        let mut output = std::io::Cursor::new(vec![]);
        let cache_file = NamedTempFile::new().unwrap();
        let mut feeder: Feeder<_, std::io::Cursor<Vec<u8>>, String> = Feeder::new(
            &[],
            Duration::from_millis(1),
            Duration::from_millis(1),
            cache_file,
            None,
            &mut output,
        );
        feeder
            .process_things(vec![good_scrape(0), bad_scrape(0)])
            .unwrap();
        assert_eq!(output.get_ref(), "Hi\n".as_bytes());
    }

    #[test]
    fn process_err_err() {
        let mut output = std::io::Cursor::new(vec![]);
        let cache_file = NamedTempFile::new().unwrap();
        let mut feeder: Feeder<_, std::io::Cursor<Vec<u8>>, String> = Feeder::new(
            &[],
            Duration::from_millis(1),
            Duration::from_millis(1),
            cache_file,
            None,
            &mut output,
        );
        feeder
            .process_things(vec![bad_scrape(0), bad_scrape(0)])
            .unwrap();
        assert_eq!(output.get_ref(), &Vec::new());
    }
}
