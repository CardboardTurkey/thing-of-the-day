use clap::{crate_authors, Parser};
use duration_string::DurationString;

#[derive(Parser, Debug)]
#[clap(author=crate_authors!(), version, about, name = "oh nana what's my name")]
/// What it do baby
pub(crate) struct Options {
    /// Delay between item cycle. Specify duration according to https://github.com/Ronniskansing/duration-string.
    #[arg(short, long)]
    pub(crate) delay: DurationString,
    /// Number of characters before truncating and adding a '...'.
    #[arg(short, long, value_parser = clap::value_parser!(u64).range(3..))]
    pub(crate) wordwrap: Option<u64>,
    /// Verbosity
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub(crate) verbosity: u8,
}
